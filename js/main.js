var dsnv = [];
var dataJson = localStorage.getItem("DSNV_LOCAL");
if (dataJson != null) {
  var dataArr = JSON.parse(dataJson);
  for (var i = 0; i < dataArr.length; i++) {
    var item = dataArr[i];
    var nv = new Nhanvien(
      item.tk,
      item.ht,
      item.email,
      item.mk,
      item.ngaylam,
      item.lcb,
      item.cv,
      item.glt
    );
    dsnv.push(nv);
  }
  renderDSNV(dsnv);
}
function themNV() {
  resetForm();
  var nv = layThongTinTuForm();
  var isValid =
    kiemTraRong("tbTKNV", nv.tk) &
    kiemTraRong("tbTen", nv.ht) &
    kiemTraRong("tbEmail", nv.email) &
    kiemTraRong("tbMatKhau", nv.mk) &
    kiemTraRong("tbLuongCB", nv.lcb) &
    kiemTraRong("tbChucVu", nv.cv) &
    kiemTraRong("tbGiolam", nv.glt);
  if (isValid) {
    dsnv.push(nv);
    var dataJson = JSON.stringify(dsnv);
    localStorage.setItem("DSNV_LOCAL", dataJson);
    renderDSNV(dsnv);
  }
}
function xoaNV(id) {
  var viTri = dsnv.findIndex(function (item) {
    return item.tk == id;
  });
  if (viTri != -1) {
    dsnv.splice(viTri, 1);
    console.log(dsnv);
    var dataJson = JSON.stringify(dsnv);
    localStorage.setItem("DSNV_LOCAL", dataJson);
    renderDSNV(dsnv);
  }
}
function suaNV(id) {
  var viTri = dsnv.findIndex(function (item) {
    return item.tk == id;
  });
  if (viTri != -1) {
    document.getElementById("tknv").disabled = true;
    showThongTinLenForm(dsnv[viTri]);
  }
}
function updateNV() {
  document.getElementById("tknv").disabled = false;
  var nv = layThongTinTuForm();
  var viTri = dsnv.findIndex(function (item) {
    return item.tk == nv.tk;
  });
  if (viTri != -1) {
    dsnv[viTri] = nv;
    var dataJson = JSON.stringify(dsnv);
    localStorage.setItem("DSNV_LOCAL", dataJson);
    renderDSNV(dsnv);
    resetForm();
  }
}
function resetForm() {
  document.getElementById("QLNVform").reset();
}
