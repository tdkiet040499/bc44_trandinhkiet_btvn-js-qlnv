var showMessage = function (id, message) {
    document.getElementById(id).innerHTML = `<strong>${message}</strong>`;
  }
  
var kiemTraRong = function (idErr, value) {
  if (value.length == 0) {
    showMessage(idErr, "Trường này không được trống");
    return false;
  } else {
    showMessage(idErr, "");
    return true;
  }
};
var kiemTraEmail = function (email) {
  const re =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  if (re.test(email)) {
    showMessage("tbEmail", "");
    return true;
  } else {
    showMessage("tbEmail", "email không hợp lệ");

    return false;
  }
};
var kiemTratk = function (){
    var vtk = getValue("tknv");
    if(vtk.length < 6 || vtk.length >8){
        showMessage('tknv', 'Vui lòng kiểm tra lại Username');
        return false;
    }else{
        return true;
    }
}