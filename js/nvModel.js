function Nhanvien(_taikhoan,_hoten,_email,_matkhau,_ngaylam,_luongcoban,_chucvu,_giolamthang){
    this.tk = _taikhoan;
    this.ht = _hoten;
    this.email = _email;
    this.mk = _matkhau;
    this.ngaylam = _ngaylam;
    this.lcb = _luongcoban;
    this.cv = _chucvu;
    this.glt = _giolamthang;
    this.tongLuong = function(){
        if(this.cv == 2){
            return this.lcb*3;
        }else if(this.cv == 3){
            return this.lcb*2;
        }else if (this.cv == 4){
            return this.lcb*1;
        }
    };
    this.loaiNV = function(){
        if (this.glt >= 192) {
          return "Nhan vien xuat sac";
        } else if (this.glt >= 176) {
          return "Nhan vien gioi";
        } else if (this.glt >= 160) {
          return "Nhan vien kha";
        }else {
          return "Nhan vien trung binh";
        }
    };
    this.chucVu = function(){
      if (this.cv == 2) {
        return "Sep";
      } else if (this.cv == 3) {
        return "Truong phong";
      } else if (this.cv == 4) {
        return "Nhan Vien";
      }
    };
}