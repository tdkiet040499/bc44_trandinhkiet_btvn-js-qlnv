function layThongTinTuForm() {
  var tk = document.getElementById("tknv").value;
  var ht = document.getElementById("name").value;
  var email = document.getElementById("email").value;
  var pass = document.getElementById("password").value;
  var ngayLam = document.getElementById("datepicker").value;
  var lCB = document.getElementById("luongCB").value;
  var cVu = document.getElementById("chucvu").value * 1;
  var gioLam = document.getElementById("gioLam").value * 1;
  var nv = new Nhanvien(tk, ht, email, pass, ngayLam, lCB, cVu, gioLam);
  return nv;
}
function renderDSNV(arrNV) {
  var contentHTML = "";
  for (var i = 0; i < arrNV.length; i++) {
    var nv = arrNV[i];
    var contentStr = ` <tr>
            <td>${nv.tk}</td>
            <td>${nv.ht}</td>
            <td>${nv.email}</td>
            <td>${nv.ngayLam}</td>
            <td>${nv.chucVu()}</td>
            <td> ${nv.tongLuong()}</td>
            <td> ${nv.loaiNV()}</td>
            <td>
               <button onclick="xoaNV(${
                 nv.tk
               })" class="btn btn-danger" >Xoá</button>
            <button onclick="suaNV(${
              nv.tk
            })" class="btn btn-secondary" data-toggle="modal"
                    data-target="#myModal" >Sua</button>
            </td>

    </tr> `;
    contentHTML += contentStr;
  }
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}
function showThongTinLenForm(nv) {
  document.getElementById("tknv").value = nv.tk;
  document.getElementById("name").value = nv.ht;
  document.getElementById("email").value = nv.email;
  document.getElementById("password").value = nv.mk;
  document.getElementById("datepicker").value = nv.ngaylam;
  document.getElementById("luongCB").value = nv.lcb;
  document.getElementById("chucvu").value = nv.cv;
  document.getElementById("gioLam").value = nv.glt;
}
